import React from 'react';
import {
    ButtonGroup,Button,Label,Card, CardHeader, CardBody, CardFooter, CardTitle, Row, Col, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, Table,
} from 'reactstrap';
// react plugin used to create charts
import { Line, Bar } from 'react-chartjs-2';
// function that returns a color based on an interval of numbers

import {
    PanelHeader, Stats, CardCategory, Tasks
} from 'components';

import {
    dashboardPanelChart,
    dashboardShippedProductsChart,
    dashboardAllProductsChart,
    dashboard24HoursPerformanceChart
} from 'variables/charts.jsx';

import { tasks } from 'variables/general.jsx';
import { 
  Container, InputGroup, InputGroupAddon, Input
} from 'reactstrap';

class Dashboard extends React.Component{
    render(){
        return (
            <div>
                <PanelHeader
                    size="sm"
                    content={
                        <p></p>
                    }
                />
                <div className="content">
                    <Row>
                        <Col xs={12} md={4}>
                            <Card className="card-chart">
                                <CardHeader>
                                    <CardCategory>No of Sites</CardCategory>
                                    <CardTitle>Number of Sites</CardTitle>
                                </CardHeader>
                                <CardBody>
                                    <div>
                                      <form>
                                          <InputGroup className="no-border">
                                              <Input placeholder="" type="number" />
                                              {/*<InputGroupAddon><i className="now-ui-icons ui-1_zoom-bold"></i></InputGroupAddon>*/}
                                          </InputGroup>
                                          
                                              <Label check>
                                                <Input type="checkbox" />{' '}
                                                Random
                                              </Label>
                                      </form>
                                    </div>
                                </CardBody>
                                <CardFooter>
                                    
                                </CardFooter>
                            </Card>
                        </Col>
                        <Col xs={12} md={4}>
                            <Card className="card-chart">
                                <CardHeader>
                                    <CardCategory>Environment Condition</CardCategory>
                                    <CardTitle>Site Condition</CardTitle>
                                </CardHeader>
                                <CardBody>
                                    <div>
                                      <form>
                                          <ButtonGroup>
                                            <Button color="success">Good</Button>{' '}
                                            <Button color="info">Medium</Button>{' '}
                                            <Button color="danger">Bad</Button>
                                          </ButtonGroup>
                                          <div>
                                            <Label check>
                                              <Input type="checkbox" />{' '}
                                              Random
                                            </Label>
                                          </div>
                                      </form>
                                    </div>
                                </CardBody>
                                <CardFooter>
                                    
                                </CardFooter>
                            </Card>
                        </Col>
                        <Col xs={12} md={4}>
                            <Card className="card-chart">
                                <CardHeader>
                                    <CardCategory>Budget</CardCategory>
                                    <CardTitle>Budget</CardTitle>
                                </CardHeader>
                                <CardBody>
                                    <div>
                                      <form>
                                          <InputGroup className="no-border">
                                              <Input placeholder="" type="number" />
                                              {/*<InputGroupAddon><i className="now-ui-icons ui-1_zoom-bold"></i></InputGroupAddon>*/}
                                          </InputGroup>
                                          <ButtonGroup>
                                            <Button color="success">High</Button>{' '}
                                            <Button color="info">Med</Button>{' '}
                                            <Button color="danger">Low</Button>
                                          </ButtonGroup>
                                          <div>
                                            <Label check>
                                              <Input type="checkbox" />{' '}
                                              Random
                                            </Label>
                                          </div>
                                      </form>
                                    </div>
                                </CardBody>
                                <CardFooter>
                                    
                                </CardFooter>
                            </Card>
                        </Col>
                        <Col xs={12} md={7}>
                            <Card className="card-chart">
                                <CardHeader>
                                    <CardCategory>Oil Price</CardCategory>
                                    <CardTitle className="center">Oil Price</CardTitle>
                                </CardHeader>
                                <CardBody>
                                    <div>
                                      <form>
                                          <ButtonGroup size="md">
                                            <Button color="success">High</Button>{' '}
                                            <Button color="info">Med</Button>{' '}
                                            <Button color="danger">Low</Button>
                                          </ButtonGroup>
                                          <div>
                                            <Label check>
                                              <Input type="checkbox" />{' '}
                                              Random
                                            </Label>
                                          </div>
                                      </form>
                                    </div>
                                </CardBody>
                                <CardFooter>
                                </CardFooter>
                            </Card>
                        </Col>
                    </Row>
                    <Button size="" color="info" style={{borderRadius: '50%', width: 75, height: 75, float: 'right'}}><i className="now-ui-icons objects_spaceship"></i></Button>
                    {/*<Row>
                      <Col xs={12} md={6}>
                        <Card className="card-tasks">
                          <CardHeader>
                            <CardTitle>Tasks</CardTitle>
                            <p className="category">Backend Development</p>
                          </CardHeader>
                          <CardBody>
                            <Tasks tasks={tasks}/>
                          </CardBody>
                          <CardFooter>
                            <hr />
                            <Stats>
                                {[
                                    { i: "now-ui-icons loader_refresh spin", t: "Updated 3 minutes ago"}
                                ]}
                            </Stats>
                          </CardFooter>
                        </Card>
                      </Col>
                      <Col xs={12} md={6}>
                        <Card>
                          <CardHeader>
                            <CardTitle>Employees Stats</CardTitle>
                            <p className="category">All Persons List</p>
                          </CardHeader>
                          <CardBody>
                            <Table responsive>
                              <thead className=" text-primary">
                                <tr>
                                  <th>Name</th>
                                <th>Country</th>
                                <th>City</th>
                                <th className="text-right">Salary</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>Dakota Rice</td>
                                  <td>Niger</td>
                                  <td>Oud-Turnhout</td>
                                  <td className="text-right">$36,738</td>
                                </tr>
                                <tr>
                                  <td>Minerva Hooper</td>
                                  <td>Curaçao</td>
                                  <td>Sinaai-Waas</td>
                                  <td className="text-right">$23,789</td>
                                </tr>
                                <tr>
                                  <td>Sage Rodriguez</td>
                                  <td>Netherlands</td>
                                  <td>Baileux</td>
                                  <td className="text-right">$56,142</td>
                                </tr>
                                <tr>
                                  <td>Doris Greene</td>
                                  <td>Malawi</td>
                                  <td>Feldkirchen in Kärnten</td>
                                  <td className="text-right">$63,542</td>
                                </tr>
                                <tr>
                                  <td>Mason Porter</td>
                                  <td>Chile</td>
                                  <td>Gloucester</td>
                                  <td className="text-right">$78,615</td>
                                </tr>
                              </tbody>
                            </Table>
                          </CardBody>
                        </Card>
                      </Col>
                    </Row>*/}
                </div>
            </div>
        );
    }
}

export default Dashboard;
