import React from 'react';
import {
    Card, CardHeader, CardBody, Row, Col,ButtonGroup,Button,Label, CardFooter, CardTitle, 
} from 'reactstrap';

import { PanelHeader } from 'components';

import icons from 'variables/icons';
import {
    dashboardPanelChart,
    dashboardShippedProductsChart,
    dashboardAllProductsChart,
    dashboard24HoursPerformanceChart
} from 'variables/charts.jsx';
import { Line, Bar } from 'react-chartjs-2';

class Simulation extends React.Component{
    render(){
        return (
            <div>
                <PanelHeader
                    size="lg"
                    content={
                        <Bar data={dashboard24HoursPerformanceChart.data} options={dashboard24HoursPerformanceChart.options} />
                    }
                />
                <div className="content">
                    <Row>
                        <Col md={12}>
                            <Card>
                                <CardHeader>
                                    <h5 className="title"></h5>
                                    <p className="category"></p>
                                </CardHeader>
                                <CardBody className="all-icons">
                                    <Row>
                                        <Col md={12}>
                                            <Card>
                                                <CardHeader>
                                                    <h5 className="title">Stage 1</h5>
                                                    <p className="category"></p>
                                                </CardHeader>
                                                <CardBody className="all-icons">
                                                    <Row>
                                                        <Bar data={dashboard24HoursPerformanceChart.data} options={dashboard24HoursPerformanceChart.options} />
                                                    </Row>
                                                </CardBody>
                                            </Card>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={12}>
                                                <CardHeader>
                                                    <h5 className="title">Stage 2</h5>
                                                    <p className="category"></p>
                                                </CardHeader>
                                                <CardBody className="all-icons">
                                                    <Row>
                                                        <Bar data={dashboard24HoursPerformanceChart.data} options={dashboard24HoursPerformanceChart.options} />
                                                    </Row>
                                                </CardBody>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={12}>
                                            <Card>
                                                <CardHeader>
                                                    <h5 className="title">Stage 3</h5>
                                                    <p className="category"></p>
                                                </CardHeader>
                                                <CardBody className="all-icons">
                                                    <Row>
                                                        <Bar data={dashboard24HoursPerformanceChart.data} options={dashboard24HoursPerformanceChart.options} />
                                                    </Row>
                                                </CardBody>
                                            </Card>
                                        </Col>
                                    </Row>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

export default Simulation;
